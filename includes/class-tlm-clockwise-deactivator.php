<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://targetlocalmarketing.com
 * @since      1.0.0
 *
 * @package    Tlm_Clockwise
 * @subpackage Tlm_Clockwise/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Tlm_Clockwise
 * @subpackage Tlm_Clockwise/includes
 * @author     Kirk Medsker <kirk@targetlocalmarketing.com>
 */
class Tlm_Clockwise_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
