<?php

/**
 * Fired during plugin activation
 *
 * @link       https://targetlocalmarketing.com
 * @since      1.0.0
 *
 * @package    Tlm_Clockwise
 * @subpackage Tlm_Clockwise/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Tlm_Clockwise
 * @subpackage Tlm_Clockwise/includes
 * @author     Kirk Medsker <kirk@targetlocalmarketing.com>
 */
class Tlm_Clockwise_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
