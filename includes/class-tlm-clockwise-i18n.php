<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://targetlocalmarketing.com
 * @since      1.0.0
 *
 * @package    Tlm_Clockwise
 * @subpackage Tlm_Clockwise/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Tlm_Clockwise
 * @subpackage Tlm_Clockwise/includes
 * @author     Kirk Medsker <kirk@targetlocalmarketing.com>
 */
class Tlm_Clockwise_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'tlm-clockwise',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
