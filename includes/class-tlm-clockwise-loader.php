<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       https://targetlocalmarketing.com
 * @since      1.0.0
 *
 * @package    Tlm_Clockwise
 * @subpackage Tlm_Clockwise/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Tlm_Clockwise
 * @subpackage Tlm_Clockwise/includes
 * @author     Kirk Medsker <kirk@targetlocalmarketing.com>
 */
class Tlm_Clockwise_Loader {

	/**
	 * The array of actions registered with WordPress.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      array    $actions    The actions registered with WordPress to fire when the plugin loads.
	 */
	protected $actions;

	/**
	 * The array of filters registered with WordPress.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      array    $filters    The filters registered with WordPress to fire when the plugin loads.
	 */
	protected $filters;

	/**
	 * Initialize the collections used to maintain the actions and filters.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->actions = array();
		$this->filters = array();

	}

	/**
	 * Add a new action to the collection to be registered with WordPress.
	 *
	 * @since    1.0.0
	 * @param    string               $hook             The name of the WordPress action that is being registered.
	 * @param    object               $component        A reference to the instance of the object on which the action is defined.
	 * @param    string               $callback         The name of the function definition on the $component.
	 * @param    int                  $priority         Optional. The priority at which the function should be fired. Default is 10.
	 * @param    int                  $accepted_args    Optional. The number of arguments that should be passed to the $callback. Default is 1.
	 */
	public function add_action( $hook, $component, $callback, $priority = 10, $accepted_args = 1 ) {
		$this->actions = $this->add( $this->actions, $hook, $component, $callback, $priority, $accepted_args );
	}

	/**
	 * Add a new filter to the collection to be registered with WordPress.
	 *
	 * @since    1.0.0
	 * @param    string               $hook             The name of the WordPress filter that is being registered.
	 * @param    object               $component        A reference to the instance of the object on which the filter is defined.
	 * @param    string               $callback         The name of the function definition on the $component.
	 * @param    int                  $priority         Optional. The priority at which the function should be fired. Default is 10.
	 * @param    int                  $accepted_args    Optional. The number of arguments that should be passed to the $callback. Default is 1
	 */
	public function add_filter( $hook, $component, $callback, $priority = 10, $accepted_args = 1 ) {
		$this->filters = $this->add( $this->filters, $hook, $component, $callback, $priority, $accepted_args );
	}

	/**
	 * A utility function that is used to register the actions and hooks into a single
	 * collection.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @param    array                $hooks            The collection of hooks that is being registered (that is, actions or filters).
	 * @param    string               $hook             The name of the WordPress filter that is being registered.
	 * @param    object               $component        A reference to the instance of the object on which the filter is defined.
	 * @param    string               $callback         The name of the function definition on the $component.
	 * @param    int                  $priority         The priority at which the function should be fired.
	 * @param    int                  $accepted_args    The number of arguments that should be passed to the $callback.
	 * @return   array                                  The collection of actions and filters registered with WordPress.
	 */
	private function add( $hooks, $hook, $component, $callback, $priority, $accepted_args ) {

		$hooks[] = array(
			'hook'          => $hook,
			'component'     => $component,
			'callback'      => $callback,
			'priority'      => $priority,
			'accepted_args' => $accepted_args
		);

		return $hooks;

	}

	/**
	 * Register the filters and actions with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {

		foreach ( $this->filters as $hook ) {
			add_filter( $hook['hook'], array( $hook['component'], $hook['callback'] ), $hook['priority'], $hook['accepted_args'] );
		}

		foreach ( $this->actions as $hook ) {
			add_action( $hook['hook'], array( $hook['component'], $hook['callback'] ), $hook['priority'], $hook['accepted_args'] );
		}
		// Usage:
		// [clockwise id='293']
		//Notes: All CSS except the scoped fusion button styling is defined in the .css file under public/css. For simplicity/dev speed, override specific elements with scoped in the HTML rather than in the .css file. Not really best practices, but, will save us a good 30 minutes of redefining all of the CSS classes.
		//More info: https://css-tricks.com/saving-the-day-with-scoped-css/
		function clockwise_function( $atts){
			$atts = shortcode_atts(
				array(
					'id' => '293'
				), $atts, 'clockwise' );

				return "<div class=\"clockwise-container\"><h2 class=\"clockwise-h2\"><img src=\"/wp-content/plugins/tlm-clockwise/public/clock2.png\" alt=\"\" /> Skip the Wait</h2><p><h3 class=\"clockwise-h3\">Current wait is <div id=\"current-wait-4-$atts[id]\" class=\"clockwise-waitbox\"></div> minutes.</h3></p>

		<center><ul class=\"fusion-checklist fusion-checklist-1\" class=\"clockwise-patients-ul\"><li class=\"fusion-li-item\"><div class=\"fusion-li-item-content\" class=\"clockwise-patients-li\"><i class=\"fa fa-chevron-right\"></i> There are <div id=\"patients-in-line-$atts[id]\" class=\"clockwise-patients-bold\"></div> patients in line.</div></li></ul><center><div class=\"fusion-button-wrapper\"><style type=\"text/css\" scoped=\"scoped\">.fusion-button.button-10 .fusion-button-text, .fusion-button.button-10 i {color:#ffffff;}.fusion-button.button-10 {border-width:0px;border-color:#ffffff;}.fusion-button.button-10 .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-10:hover .fusion-button-text, .fusion-button.button-10:hover i,.fusion-button.button-10:focus .fusion-button-text, .fusion-button.button-10:focus i,.fusion-button.button-10:active .fusion-button-text, .fusion-button.button-10:active{color:#000000;}.fusion-button.button-10:hover, .fusion-button.button-10:focus, .fusion-button.button-10:active{border-width:0px;border-color:#000000;}.fusion-button.button-10:hover .fusion-button-icon-divider, .fusion-button.button-10:hover .fusion-button-icon-divider, .fusion-button.button-10:active .fusion-button-icon-divider{border-color:#000000;}.fusion-button.button-10{background: #510429;}.fusion-button.button-10:hover,.button-10:focus,.fusion-button.button-10:active{background: #bc8095;}.fusion-button.button-10{width:auto;}</style><a class=\"fusion-button button-flat fusion-button-square button-xlarge button-custom button-10\" target=\"_blank\" href=\"https://www.clockwisemd.com/hospitals/$atts[id]/appointments/new\"><span class=\"fusion-button-text\">Reserve My Spot</span></a></div></div></center></center>
		<script>
		jQuery(document).ready(function($) {

					var HOSPITAL_ID = $atts[id];
					var WAIT_FETCH_OBJECTS = [
							{ hospitalId: HOSPITAL_ID,
								timeType:   'hospitalWait',
								selector:   '#current-wait-4-$atts[id]' },
							{ hospitalId: HOSPITAL_ID,
								timeType:   'hospitalPatientsInLine',
								selector:   '#patients-in-line-$atts[id]' }
					];
					beginWaitTimeQuerying(WAIT_FETCH_OBJECTS);
		});
		</script>";

		}
		add_shortcode( 'clockwise', 'clockwise_function' );
	}

}
